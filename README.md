# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

* Quick summary: LMVD and NTF implementation
* Version 1.3.0

### How do I get set up? ###

* Need lateed Java SDK 1.8.0_65 up
* You can download java from [here](http://java.com/zh_TW/download/manual.jsp).

### How to run program ###
Threre are three kinds of method, you can choose one, I recommend first one.

1. (Recommand) Open command line tool, go to path: ../dist, you will see a jar here. If you already install java on comuter, then tpye **java -jar LMVD_NTF_Run.jar**, you will see result, console information and image in 'ResultImage' folder.
2. In 'exe' folder there are a lena.jpg, LMVD_NTF_Run.exe, 'ResultImage' folder by default. Double Click LMVD_NTF_Run.exe, you will see result and output file in 'ResultImage' folder.
3. Use NetBeans 8.1 (if you want to compile it)

### Contribution guidelines ###
* Bo Cheng Huang

![NTF_Result.png](https://bitbucket.org/repo/L9qAqM/images/102061573-NTF_Result.png)